import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule,{
    transport: Transport.RMQ,
    options: {
      urls: ['amqps://lyvrsxwr:UFwp4fbJvgUOysG7BjzL1xSxffqf4CNh@owl.rmq.cloudamqp.com/lyvrsxwr'],
      queue: 'main_queue',
      queueOptions: {
        durable: false
      },
    }
  });
  await app.listen(()=>{
    console.log('Microservice is listening')
  });
}
bootstrap();

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const microservices_1 = require("@nestjs/microservices");
const app_module_1 = require("./app.module");
async function bootstrap() {
    const app = await core_1.NestFactory.createMicroservice(app_module_1.AppModule, {
        transport: microservices_1.Transport.RMQ,
        options: {
            urls: ['amqps://lyvrsxwr:UFwp4fbJvgUOysG7BjzL1xSxffqf4CNh@owl.rmq.cloudamqp.com/lyvrsxwr'],
            queue: 'main_queue',
            queueOptions: {
                durable: false
            },
        }
    });
    await app.listen(() => {
        console.log('Microservice is listening');
    });
}
bootstrap();
//# sourceMappingURL=listener.js.map